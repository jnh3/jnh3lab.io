= Building picolibc for GCC and Cortex-M Microcontrollers

:date: October 11, 2021
:tags: libc,picolibc
:category: C Libraries
:slug: building-picolibc
:author: John Haroian
:email: <john@haroian.net>
:revision:  v1.0
:summary: Building Picolibc on macOS

I found picolibc back in November 2020 while looking to put together a tool chain for my new M1 based Macbook Pro.  The idea was to use clang (LLVM) as gcc was not available (and still is not) as a native M1 based program.

My interest in the library was its focus on smaller platforms like the Cortex-M.  Small executable sizes and solid testing all sounded good for my needs.  I was able to successfully compile it for clang (LLVM) but then had more issues with getting the rest of the tool chain setup and independent of all the necessary gcc pieces that clang (LLVM) still requires.  The project stalled as life continued to happen.

Fast forward to the Embedded Linux Conference in September 2021 and I was able to attend Kieth Packard's talk on his picolibc work.  The presentation was excellent and we exchanged comments after the talk.  One need that he identified was more linker scripts for specific Cortex-M (and other architectures) microcontrollers.  That inspired me to go back to gcc running via Rosetta 2 which is still very performant and put together some linker scripts for the Microchip line of Cortex-M devices.

The first step for using picolibc is to clone its repository and build the library for the Cortex-M architecture.

== Requirements
- gcc needs to be installed (this works for both Intel and M1 based macOS computers and likely any Linux machines as well).  I used Homebrew and installed the cask with the following command:

----
brew install gcc-arm-embedded
----

When installed and located in the path, the following commands will work as follows:

----
jharoian@Johns-MBP:~|⇒  which arm-none-eabi-gcc
/opt/homebrew/bin/arm-none-eabi-gcc
----
----
jharoian@Johns-MBP:~|⇒  arm-none-eabi-gcc --version
arm-none-eabi-gcc (GNU Arm Embedded Toolchain 10.3-2021.07) 10.3.1 20210621 (release)
Copyright (C) 2020 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
----

- Install meson and ninja as the build system for picolibc.  Meson was also installed with Homebrew which pulls in the other dependencies (ninja and python)

----
brew install meson
----

Once installed, check that all is ok by launching both and checking the version numbers.  

----
[venv-setup-embedded-tools]jharoian@Johns-MBP:~/Projects/setup-m1-embedded-tools|main⚡ 
⇒  meson --version
0.59.2
[venv-setup-embedded-tools]jharoian@Johns-MBP:~/Projects/setup-m1-embedded-tools|main⚡ 
⇒  ninja --version
1.10.2
----

- Clone the picolibc repository:

----
git clone https://github.com/picolibc/picolibc.git
----

- Change into the 'picolibc' directory and create a build directory as meson does not allow in-tree building.  The build directory name does not matter, but since the project can be built for several different architectures, the example below uses 'build-arm' for the build directory.

----
[venv-setup-embedded-tools]jharoian@Johns-MBP:~/Projects/setup-m1-embedded-tools|main⚡ 
⇒  cd picolibc 
[venv-setup-embedded-tools]jharoian@Johns-MBP:~/Projects/setup-m1-embedded-tools/picolibc|main 
⇒  mkdir build-arm
[venv-setup-embedded-tools]jharoian@Johns-MBP:~/Projects/setup-m1-embedded-tools/picolibc|main 
⇒  cd build-arm 
----

- The 'scripts' directory in the picolibc project contains a large number of scripts to build the library for a number of architectures and OS combinations.  In our case, we need the arm and bare metal option.  Once the script below launches, the output has a large number of warnings that are successfully ignored as they describe a deprecated feature that is still supported.  These will need to be cleaned up prior to meson removing the feature.  

----
../scripts/do-arm-configure 
----

- Build the library with the command below.  On a M1 Macbook Pro, the build took just over 8 
minutes.

----
ninja
----

Still hoping that a clang tool chain can be put together independently of gcc so that the compile times can come down.  Some Intel processes running during the picolibc build:

.macOS Activity Monitor - gcc is Intel code
image::images/picolibc_Activity_Monitor.jpg[Picture of macOS Activity Monitor - gcc is Intel code]

Even with the Rosetta 2 burden, the compile time was fast given that the library was build for all the multilib variations of arm-none-eabi-gcc.  Meaning, all the Cortex-M variants were built.  All 3466 targets due to variations of floating point libraries and versions of printf libraries, semi-hosting support, etc.

- Install the built libraries so they can be used by arm-none-eabi-gcc (the gcc cross compiler for Cortex-M ARM microcontrollers)

----
sudo ninja install
----

The install target copies the built libraries to /usr/local/picolibc hence the sudo requirement.  The libraries are available for all users and the cloned directory can be removed at this point.

== Testing the picolibc library
The final step is to compile a small test program and make sure that the tool chain works with the new library.

In any project directory of your choosing, create the following simple C program:

----
#include <stdio.h>
void main(void)
{
  printf("hello, world\n");
}
----

Also, create the following linker script:
----
__flash =      0x00000000;
__flash_size = 0x00004000;
__ram =        0x20000000;
__ram_size   = 0x00001000;
INCLUDE picolibc.ld
----

This linker script works for the Microchip SAMD11 microcontroller.

The directory contents should look like the following:
----
jharoian@Johns-MBP:~/Projects/picolibc-test|
⇒  ls -l
total 16
-rw-r--r--  1 jharoian  staff   68 Oct 10 21:58 hello.c
-rw-r--r--  1 jharoian  staff  129 Oct 10 22:00 samd11.ld
----

Execute the following lines to test the compilation:

----
arm-none-eabi-gcc --specs=picolibc.specs --oslib=semihost --crt0=minimal \
                  -Os -g -mcpu=cortex-m0plus -Tsamd11.ld -o hello.elf hello.c
----

The result is the following:
----
-rwxr-xr-x  1 jharoian  staff  144236 Oct 10 22:02 hello.elf
----

The elf file includes the debug information.  The actual executable (use 'arm-none-eabi-objdump -d hello.elf') is located from 0x08000000 to 0x0800012c. That is 300 bytes decimal including the printf routine which reduced down to a series of putc() calls out the interface to the host system due to the semihost option.  According to Keith's presentation, this program uses 24 bytes of RAM on the stack when compiled for the Cortex-M3. (Note - correct this for SAMD11)

== Running the program on the target
While having a simple program compile without error is nice, most would like to see it run on a target.  To accomplish this, openocd was used to pass the program down to a target.

Openocd was, again, installed with Homebrew.
----
brew install openocd
----

The target used was the SAMD11 Xplained evaluation board which is available from any Microchip authorized distributor.  The following command executes the program:
----
openocd -f board/atmel_samd11_xplained_pro.cfg \
   -c "program hello.elf" \
   -c 'arm semihosting enable' \
   -c 'reset run'
----

Better yet, create a user configuration file for openocd.  Place the following into a file called openocd.cfg
----
source [find board/atmel_samd11_xplained_pro.cfg]
adapter speed 5000
program hello.elf verify
reset halt
arm semihosting enable
arm semihosting_fileio enable
----

Then, launch openocd
----
jharoian@Johns-MBP:~/Projects/picolibc-test|
⇒  openocd
Open On-Chip Debugger 0.11.0
Licensed under GNU GPL v2
For bug reports, read
    http://openocd.org/doc/doxygen/bugs.html
Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: FW Version = 03.25.01B6
Info : CMSIS-DAP: Serial# = ATML2178051800002672
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 1 SWDIO/TMS = 1 TDI = 1 TDO = 1 nTRST = 0 nRESET = 1
Info : CMSIS-DAP: Interface ready
Info : clock speed 5000 kHz
Info : SWD DPIDR 0x0bc11477
Info : at91samd11d14.cpu: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for at91samd11d14.cpu on 3333
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread 
xPSR: 0x61000000 pc: 0x00000050 msp: 0x20001000
** Programming Started **
Info : SAMD MCU: SAMD11D14AM (16KB Flash, 4KB RAM)
** Programming Finished **
** Verify Started **
** Verified OK **
target halted due to debug-request, current mode: Thread 
xPSR: 0x61000000 pc: 0x00000050 msp: 0x20001000
semihosting fileio is enabled

Info : Listening on port 6666 for tcl connections
Info : Listening on port 4444 for telnet connections
----

At this point, the program has been flashed into the microcontroller and the part has halted in the init code (post reset) waiting for gdb to connect.

To launch gdb, start another shell in a tab or another window.  Change into the same project directory and place the following script into a file called .gdbinit.  On macOS and Linux systems, this is a hidden file.  To see it in the directory list, use the following command:
----
jharoian@Johns-MBP:~/Projects/picolibc-test|
⇒  ls -la
total 320
drwxr-xr-x   7 jharoian  staff     224 Oct 11 22:42 .
drwxr-xr-x  36 jharoian  staff    1152 Oct 10 22:00 ..
-rw-r--r--   1 jharoian  staff      44 Oct 11 22:43 .gdbinit
-rw-r--r--   1 jharoian  staff      68 Oct 10 21:58 hello.c
-rwxr-xr-x   1 jharoian  staff  144236 Oct 10 22:23 hello.elf
-rw-r--r--   1 jharoian  staff     159 Oct 11 22:39 openocd.cfg
-rw-r--r--   1 jharoian  staff     129 Oct 10 22:23 samd11.ld
----

Once .gdbinit is created, gdb can be launched as follows:
----
jharoian@Johns-MBP:~/Projects/picolibc-test|
⇒  arm-none-eabi-gdb hello.elf
GNU gdb (GNU Arm Embedded Toolchain 10.3-2021.07) 10.2.90.20210621-git
Copyright (C) 2021 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "--host=x86_64-apple-darwin10 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from hello.elf...
_start () at ../picocrt/machine/arm/crt0.c:57
57      __asm__(".equ __my_interrupt_vector, __interrupt_vector");
Loading section .init, size 0x40 lma 0x0
Loading section .text, size 0xf0 lma 0x40
Loading section .data, size 0x10 lma 0x130
Start address 0x00000050, load size 320
Transfer rate: 1 KB/sec, 106 bytes/write.
(gdb) 
----

At this point, the program can start executing with a continue command:
----
(gdb) continue
Continuing.
hello, world
^C
Program received signal SIGINT, Interrupt.
__start () at ../picocrt/machine/arm/../../crt0.h:95
95      for(;;);
(gdb) quit
A debugging session is active.

    Inferior 1 [Remote target] will be detached.

Quit anyway? (y or n) y
Detaching from program: /Users/jharoian/Projects/picolibc-test/hello.elf, Remote target
[Inferior 1 (Remote target) detached]
jharoian@Johns-MBP:~/Projects/picolibc-test|
⇒  
----

The embedded program displays "hello, world" through the semihosting capability in the debugger setup without using a UART.  The program then exits and is put into an infinite loop by the start up code.  Issuing a CTRL-C exits the loop, and quit exits gdb.

Arriving here, the tool chain has been setup from compiler to libc to debugger.  The setup is ready for your code.

== Architectures supported 
Following the above instructions will result in a set of picolibc libraries that support the following architectures.

ARM
v5te

Cortex-M (Thumb2)
nofp            v7-a            v7-r+fp.sp      v7ve+simd       v8-m.main
v6-m            v7-a+fp         v7e-m           v8-a            v8-m.main+dp
v7              v7-a+simd       v7e-m+dp        v8-a+simd       v8-m.main+fp
v7+fp           v7-m            v7e-m+fp        v8-m.base       v8.1-m.main+mve

While noticing that there is support for Thumb2 on a number of Cortex-A ISAs, the important result to note is all Cortex-M microcontroller ISAs are covered including the latest v8-m.base and v8-m.main.  

Cortex-M0+  v6-m
Cortex-M3/4/7 v7-m
Cortex-M23 v8-m.base
Cortex-M33 v8-m.main

== Notes on Linker Script
In this example, the picolibc.ld linker script was used as a base which can cover a large number of use cases for embedded programming.  The picolibc.ld linker script needs Flash start address, Flash size, SRAM start address, and SRAM size defined by the following variables:

----
__flash =      0x08000000;
__flash_size = 0x00020000;
__ram =        0x20000000;
__ram_size   = 0x00004000;
INCLUDE picolibc.ld
----

The last line includes the picolibc.ld linker script.  It will also accept a stack size variable, +__stack_size+.  If this is not defined then the stack size defaults to 0x0800 (2048) bytes.  The start address for the stack is calculated based on the RAM requirements of the program.  The remainder of SRAM is allocated to the heap in case dynamic allocation (boo hiss!) is used or needed in the application.

== OpenOCD Notes
- board files are located in '/opt/homebrew/opt/open-ocd/share/openocd/scripts/board'
- OpenOCD User's Guide: https://openocd.org/doc-release/html/index.html

== gdb Notes
- program size verification:
----
(gdb) load
Loading section .init, size 0x40 lma 0x0
Loading section .text, size 0xf0 lma 0x40
Loading section .data, size 0x10 lma 0x130
Start address 0x00000050, *load size 320*
Transfer rate: 996 bytes/sec, 106 bytes/write.
----
- Debugging with GDB: https://sourceware.org/gdb/current/onlinedocs/gdb/

== References
Keith Packard presentation: https://keithp.com/picolibc/picolibc-2021-notes.pdf
Linaro Confluence page for ARM semihosting setup: https://linaro.atlassian.net/wiki/spaces/TCWGPUB/pages/25264430261/How+to+use+openOCD+semi-hosting+support

